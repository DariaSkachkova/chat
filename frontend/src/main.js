import { createApolloProvider } from '@vue/apollo-option'
import { createApp } from 'vue'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { WebSocketLink } from 'apollo-link-ws'
import { split } from 'apollo-link'
import { getMainDefinition } from 'apollo-utilities'
import App from './App.vue'
import router from './router'
import store from './store'

const httpLink = new HttpLink({
    uri: 'http://localhost:3000/graphql'
});

const wsLink = new WebSocketLink({
    uri: 'ws://localhost:3000/graphql',

    options: {
        reconnect: true,
    },
})

const splitLink = split(
    ({ query }) => {
        const definition = getMainDefinition(query)
        return definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
    },
    wsLink,
    httpLink
)

const apolloClient = new ApolloClient({
    link: splitLink,
    cache: new InMemoryCache()
})

const apolloProvider = createApolloProvider({
    defaultClient: apolloClient,
})

createApp(App).use(apolloProvider).use(store).use(router).mount('#app')
