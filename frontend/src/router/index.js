import { createRouter, createWebHistory } from 'vue-router'

import AppAuthPage from '../views/AppAuthPage'

const routes = [
  {
    path: '/chat',
    name: 'Chat',
    component: () => import('../views/AppChatPage.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: "Auth",
    component: AppAuthPage
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
