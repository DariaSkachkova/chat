const ROOT = 'userInfo'
const USER_NAME = 'login'
const TOKEN = 'token'

const getItemFromLocalStorage = item => {
    const userInfo = JSON.parse(localStorage.getItem(ROOT)) || {}

    return userInfo[item] || ''
}

const getTokenFromLocalStorage = () => {
    return getItemFromLocalStorage(TOKEN) || ''
}

const getUserNameFromLocalStorage = () => {
    return getItemFromLocalStorage(USER_NAME) || ''
}

const clearLocalStorage = () => localStorage.removeItem(ROOT)

const updateUserInfoToLocalStorage = userInfoAsObject => {
    localStorage.setItem(ROOT, JSON.stringify(userInfoAsObject))
}

export {
    getTokenFromLocalStorage,
    getUserNameFromLocalStorage,
    clearLocalStorage,
    updateUserInfoToLocalStorage
}