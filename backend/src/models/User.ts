import {
    BaseEntity,
    Column,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm'
import { Message } from './Message'

@Entity('users')
export class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @Column({type: 'varchar', length: 100})
    userName: string = ''
    @Column({type: 'varchar', length: 100, unique: true})
    email: string = ''
    @Column({type: 'varchar', length: 100})
    password: string = ''
    @OneToMany(() => Message, (message) => message.userId)
    messages: Message[]
}