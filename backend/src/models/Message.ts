import {
    BaseEntity,
    Column,
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    ManyToOne,
    JoinColumn
} from 'typeorm'
import { User } from './User'

@Entity('messages')
export class Message extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number
    @CreateDateColumn()
    createdAt: Date
    @Column({type: 'text'})
    text: string
    @ManyToOne(() => User, (user) => user.messages)
    @JoinColumn({name: 'userId'})
    userId: any

}