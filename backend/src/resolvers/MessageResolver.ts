import {Arg, Authorized, Ctx, Mutation, PubSubEngine, Query, Resolver, Root, Subscription} from 'type-graphql'
import { Message } from '../models/Message'
import {MessageGraphQL} from "../types/MessageGraphQL";
import {MessageInputGraphQL} from "../types/MessageInputGraphQL";
import { Inject, Service } from 'typedi'

@Service()
@Resolver()
export class MessageResolver {
    @Inject('pubSub')
    pubSub: PubSubEngine
    @Authorized()
    @Mutation(() => MessageGraphQL)
    async addMessage(
        @Arg("data",
            () => MessageInputGraphQL,
            { nullable: false })
             data: MessageInputGraphQL,
        @Ctx() ctx: any
    ){
        const newMessage = new Message()
        newMessage.text = data.text
        newMessage.createdAt = new Date()
        newMessage.userId = ctx.user.id
        await newMessage.save()
        await this.pubSub.publish('MESSAGE_UPDATE', newMessage)
        return await Message
            .createQueryBuilder("message")
            .leftJoinAndSelect("message.userId", "user")
            .where("message.id = :id", {id: newMessage.id})
            .getOne()
    }

    @Query( () => MessageGraphQL)
    async getMessageById(
        @Arg("messageId", { nullable: false }) messageId: number
    ) {
        return await Message
            .createQueryBuilder("message")
            .leftJoinAndSelect("message.userId", "user")
            .where("message.id = :id", {id: messageId})
            .getOne()
    }

    @Query(() => [MessageGraphQL])
    async getMessageList() {
        const datePoint = new Date()
        datePoint.setHours(datePoint.getHours() - 2)

        return await Message
            .createQueryBuilder("message")
            .leftJoinAndSelect("message.userId", "user")
            .where("message.createdAt > :date", {date: datePoint})
            .orderBy("message.id")
            .getMany()
    }

    @Subscription(() => MessageGraphQL, {
        topics: 'MESSAGE_UPDATE',
    })
    messageUpdate(@Root() message: Message): MessageGraphQL {
        return message as any
    }

}