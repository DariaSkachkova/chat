import { Arg, Authorized, Ctx, Mutation, Query, Resolver } from 'type-graphql'
import { User } from '../models/User'
import * as bcrypt from 'bcrypt'
import { UserRegisterInputGraphQL } from '../types/UserRegisterInputGraphQL'
import { UserLoginInputGraphQL } from '../types/UserLoginInputGraphQL'
import { UserRegister } from '../types/UserRegister'
import { Config } from "../config"
import * as jwt from 'jsonwebtoken'
import { UserGraphQL } from '../types/UserGraphQL'
import {AppContext} from "../types/AppContext";
import { Service } from 'typedi'

@Service()
@Resolver()
export class UserResolver {
    @Mutation(() => UserRegister)
    async userRegister(
        @Arg('data', () => UserRegisterInputGraphQL,{validate:{groups:['register']}})
            data: UserRegisterInputGraphQL,
        @Ctx() ctx: AppContext
    ): Promise<UserRegister> {
        let user = await User.findOne({ where: { email: data.email } })
        if (user) {
            const error = new Error(
                `User with given email (${data.email}) already exists`
            ) as any
            error.code = 422
            throw error
        }
        user = new User()
        user.userName = data.userName
        user.email = data.email
        user.password = bcrypt.hashSync(data.password, 10)
        await user.save()
        const out = new UserRegister()
        out.token = jwt.sign(
            { id: user.id, email: user.email },
            Config.secretKey,
            { expiresIn: '30 days' }
        )
        ctx.session.res.cookie('token', out.token, {
            path: '/',
        })
        out.user = user
        return out
    }
    @Mutation(() => UserRegister)
    async userLogin(
        @Arg('data', () => UserLoginInputGraphQL,{validate:false})
            data: UserLoginInputGraphQL,
        @Ctx() ctx: AppContext
    ): Promise<UserRegister> {
        let user = await User.findOne({ where: { email: data.email } })
        if (!user) {
            const error = new Error(
                `Wrong email or password`
            ) as any
            error.code = 401
            throw error
        }
        if (bcrypt.compareSync(data.password, user.password)) {
            const out = new UserRegister()
            out.token = jwt.sign(
                { id: user.id, email: user.email },
                Config.secretKey,
                { expiresIn: '30 days' }
            )
            out.user = user
            ctx.session.res.cookie('token', out.token, {
                path: '/',
            })
            return out
        }
        const error = new Error(
            `Wrong email or password`
        ) as any
        error.code = 401
        throw error
    }
}
