import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateTableMessages1640555569898 implements MigrationInterface {
    name: 'CreateTableMessages1640555569898'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE \`messages\`
             (
                 \`id\`        int      NOT NULL AUTO_INCREMENT,
                 \`createdAt\` datetime NOT NULL,
                 \`text\`      text     NOT NULL,
                 \`userId\`    int      NOT NULL,
                 PRIMARY KEY (\`id\`)
             ) ENGINE=InnoDB`
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE \`messages\``)
    }
}