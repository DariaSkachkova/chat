import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import { ApolloServer } from 'apollo-server-express'
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core'
import { buildSchema } from 'type-graphql'
import * as jwt from 'jsonwebtoken'
import { Config } from "./config"
import * as http from "http";
import * as cookieParser from 'cookie-parser'
import cookie from 'cookie'
import {
    ConnectionContext,
    SubscriptionServer,
} from 'subscriptions-transport-ws'
import { execute, subscribe } from 'graphql'
import { PubSub } from 'graphql-subscriptions'
import { Container } from 'typedi'
const cors = require("cors")


const startServer = async () => {
    const pubSub = new PubSub()
    Container.set('pubSub', pubSub)
    const app = express()
    app.use(cookieParser())
    app.use(express.json())

    app.use(cors())

    const httpServer = http.createServer(app)
    await createConnection()

    const schema = await buildSchema({
        pubSub,
        resolvers: [`${__dirname}/**/*Resolver.ts`],
        dateScalarMode: 'isoDate',
        authChecker: (resolverData, roles) => {
            return !!resolverData.context.user
        },
        container: Container,
    })

    const subscriptionServer = SubscriptionServer.create(
        {
            schema,
            execute,
            subscribe,
            async onConnect() {
                console.log('connected')
            },
            onDisconnect(){
                console.log('disconnect')
            },
        },
        {
            server: httpServer,
            path: '/graphql',
        }
    )

        const apolloServer = new ApolloServer({
            schema: schema,
            plugins: [
                ApolloServerPluginDrainHttpServer({ httpServer }),
                {
                    async serverWillStart() {
                        return {
                            async drainServer() {
                                subscriptionServer.close()
                            },
                        }
                    },
                },
            ],
            context: (session) => {
                const out: any = {}
                const token = session.req.header('Authorization')

                if (token) {
                    out.user = jwt.verify(token, Config.secretKey)
                }
                out.session = session
                return out
            },
        })
        await apolloServer.start()
        apolloServer.applyMiddleware({
            app,
            path: '/graphql',
            cors: {
                credentials: true,
                origin: function (origin: any, callback: any) {
                    callback(null, true)
                },
            },
        })
        await new Promise<void>((resolve) =>
            httpServer.listen({ port: 3000 }, () => {
                console.log('Apollo Server started')
                resolve()
            })
        )
    };


startServer().catch((e) => {
    console.error(e)
}).then(() => console.log('Express Server started'))