import { Field, InputType } from 'type-graphql'


@InputType()
export class UserLoginInputGraphQL {
    @Field()
    email: string
    @Field()
    password: string
}