import { Field, Int, ObjectType } from 'type-graphql'

@ObjectType()
export class UserGraphQL {
    @Field(() => Int)
    id: number
    @Field()
    email: string
    @Field()
    userName: string
    @Field()
    password: string
}