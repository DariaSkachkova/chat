import {Field, Int, ObjectType} from 'type-graphql'
import { User } from "../models/User"
import { UserGraphQL } from "./UserGraphQL"

@ObjectType()
export class MessageGraphQL {
    @Field(() => Int)
    id: number
    @Field()
    createdAt: Date
    @Field(() => String)
    text: string
    @Field(() => UserGraphQL)
    userId: User[]
}