import { Field, InputType } from 'type-graphql'


@InputType()
export class UserRegisterInputGraphQL {
    @Field()
    userName: string
    @Field()
    email: string
    @Field()
    password: string
}