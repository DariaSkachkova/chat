import { Field, InputType } from 'type-graphql'


@InputType()
export class MessageInputGraphQL {
    @Field()
    text: string
}